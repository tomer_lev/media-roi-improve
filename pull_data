import pandas as pd
import numpy as np
import sqlalchemy as sql
import json

#------------------------------------------------------------------------------
#                     THE FUNCTIONS 
#------------------------------------------------------------------------------
  
def create_engine():

        with open('redshift_connector/resources/config-prod.json') as json_data:
            pg_cred = json.load(json_data)['postgres_v2']
        db_pg = (
        'postgresql+psycopg2://{p[user]}:{p[password]}@{p[host]}:{p[port]}/{p[db_name]}'.format(p=pg_cred))
        pg_engine = sql.create_engine(db_pg)

        return pg_engine
    

def get_client_job_data(pg_engine):                                                   

    query_3 = """
                SELECT a.ats_req_id,
                       c.segment_id,
                       c.flight_id,
                       a.customer_id,
                       b.career_cluster,
                       a.radar_category_id,
                       c.job_board,
                       date_trunc('week', c.date)   as date,
                       count(distinct a.ats_req_id) as num_jobs,
                       sum(c.clicks_p)              AS clicks,
                       sum(c.conversions_p)         AS convs,
                       sum(c.cost)                  AS cost
                FROM (select ats_req_id,
                             customer_id,
                             radar_category_id
                      from classifier_results_radar_and_tmp_on_master_feed) a
                         JOIN (select id,
                                      career_cluster
                               from customers) b ON a.customer_id = b.id
                         JOIN (select date,
                                      customer_id,
                                      ats_req_id,
                                      segment_id,
                                      flight_id,
                                      job_board,
                                      clicks_p,
                                      conversions_p,
                                      cost
                               from panther.master_detailed_job_stats
                               where date >= '2020-01-01') c ON a.customer_id = c.customer_id AND a.ats_req_id = c.ats_req_id
                GROUP BY a.ats_req_id,
                         c.segment_id,
                         c.flight_id,
                         a.customer_id,
                         b.career_cluster,
                         a.radar_category_id,
                         c.job_board,
                         c.date;
                         """
    clicks = pd.read_sql(query_3, pg_engine)          
    
    return (clicks)


#------------------------------------------------------------------------------
#                     RUN THE FUNCTIONS 
#------------------------------------------------------------------------------
# !!! Don't forget to change the date in the WHERE clause of 'get_client_job_data' to 
#     determine the time period of the data pulled. Split the time periods over two
#     different runs - send one to clicks.csv and the other to clicks1.csv


pg_engine   = create_engine()
clicks   = get_client_job_data(pg_engine) 

clicks = clicks.replace([np.inf, -np.inf], np.nan)  # replace inf's with nan's
clicks = clicks.dropna(inplace=True)                # drop nan's

clicks.to_csv('clicks.csv')      # for 1st set of dates under the where clause in get_client_job_data
#clicks.to_csv('clicks1.csv')    # for 2nd set of dates under the where clause in get_client_job_data



